package cz.cvut.fel.pjv;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import javafx.geometry.Rectangle2D;

/**
 *
 * @author Sofia
 */
public class Sprite {
   private SpriteId id;
   private String groupName;
   private double x, y , width, height, sx, sy, fw, fh;
   private int frames;
   private int cnt;
   private boolean animate;
   private Image image;
   private String imageName;
   private int change = 0;

    public Sprite() {
    }

    
  
    /**
     *
     * @param id
     * @param imageName file name
     * @param sourcex x coordinate in source image
     * @param sourcey y coordinate in source image
     * @param source_width taked width of source image
     * @param source_height taked height of source image
     * @param final_width width in the game
     * @param final_height height in the game
     * @param frames number of frames in sprite animation
     * @param animate if true sprite will be animated, if false - static.
     */
    public Sprite(
          SpriteId id, 
          String imageName, 
          double sourcex, 
          double sourcey, 
          double source_width,
          double source_height,
          double final_width,
          double final_height,
          int frames,
          boolean animate) {
    this.id = id;
    this.groupName = String.valueOf(id);
    this.imageName = imageName;
    this.image = null;
    this.width = source_width;
    this.height = source_height;
    this.sx = sourcex;
    this.sy = sourcey;
    this.fw = final_width;
    this.fh = final_height;
    this.frames = frames;
    this.animate = animate;
    this.cnt = 0;
  }
  
  public SpriteId getId() {
    return this.id;
  }
  
  public String getGroupName(){
      return this.groupName;
  }

  public void setGroupName(String groupName) {
      this.groupName = groupName;
  }

    /**
     * update sprite
     * @param gc Graphics Context
     */
    public void render(GraphicsContext gc) {
    if (image == null){
        image = new Image(imageName);
    }
    if(this.animate){
          gc.drawImage(this.image, this.sx + this.cnt*this.width, this.sy, 
                  this.width, this.height, this.x, this.y, this.fw, this.fh);
          change++;
          if (change == 15){
                change = 0;
                cnt++;
             if (cnt >= frames){
                 cnt = 0;
             } 
          }        
    }else{
        gc.drawImage(image, sx, sy, width, height, x, y, fw, fh);
    }
  }
  
  public void setPosition(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public void increaseX(double x) {
    this.x += x;
  }

  public void increaseY(double y) {
    this.y += y;
  }

  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }

  public void setX(double x) {
    this.x = x;
  }

  public void setY(double y) {
    this.y = y;
  }
  
  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public void setAnimate(boolean animate) {
    this.animate = animate;
  }

    public boolean isAnimate() {
        return animate;
    }
  
  
    /**
     * Get rectangle bounds of a sprite.
     * @return 
     */
  public Rectangle2D getBounds() {
    return new Rectangle2D(x, y, fw, fh);
  }

    public double getFw() {
        return fw;
    }

    public void setFw(double fw) {
        this.fw = fw;
    }

    public double getFh() {
        return fh;
    }

    public void setFh(double fh) {
        this.fh = fh;
    }

    public void setSx(double sx) {
        this.sx = sx;
    }

    public void setSy(double sy) {
        this.sy = sy;
    }

    public double getSx() {
        return sx;
    }

    public double getSy() {
        return sy;
    }
    
    public int getFrames() {
        return frames;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public String getImageName() {
        return imageName;
    }
    
    
}
