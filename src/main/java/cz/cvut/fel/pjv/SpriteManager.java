package cz.cvut.fel.pjv;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sofia (based on LaSer version)
 */
public class SpriteManager {
  private Map<SpriteId, Sprite> sprites;
  
  public SpriteManager(Sprite... spriteList) {
    sprites = new HashMap<>();
    for (Sprite sprite : spriteList) {
      sprites.put(sprite.getId(), sprite);
    }
  }
  
    /**
     * Add sprite to list.
     * @param sprite
     */
    public void addSprite(Sprite sprite){
      sprites.put(sprite.getId(), sprite);
  }
  
    /**
     *
     * @return list of sprites.
     */
    public Sprite[] getSprites() {
    return sprites.values().toArray(new Sprite[sprites.size()]);
  }
  
    /**
     * 
     * @param id SpriteId
     * @return sprite by his id.
     */
    public Sprite getSprite(SpriteId id) {
    return sprites.get(id);
  }
  
    /**
     * remove sprite from list. 
     * @param id SpriteId
     */
    public void deleteSprite(SpriteId id){
      sprites.remove(id);
  }
    
    /**
     * Empty the list of sprites.
     */
    public void clearAllSprites(){
        sprites.clear();
    }
}
