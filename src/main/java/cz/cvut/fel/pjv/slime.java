package cz.cvut.fel.pjv;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import static javafx.scene.paint.Color.BLACK;
import javafx.scene.shape.Circle;

/**
 *
 * @author sonia
 */
public class slime extends Sprite {
    private double speedY;
    private double speedX;
    private boolean died;
    private Circle circleBounds; // bounds of slime are approximate to circle
    
    public slime(SpriteId id, String imageName, 
            double sourcex, 
            double sourcey, 
            double source_width,
            double source_height,
            double final_width,
            double final_height,
            int frames,
            boolean anim) {
    super(id, imageName, sourcex, sourcey, source_width, source_height, final_width, final_height, frames, anim);
    speedY = 0;
    speedX = 0;
    died = false;
    circleBounds = new Circle();
    circleBounds.setRadius(12);
    circleBounds.setCenterX(final_width/2+getX());
    circleBounds.setCenterY(getY()+final_height-circleBounds.getRadius());
  }

    public double getSpeedY() {
        return speedY;
    }

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }  

    public boolean isDied() {
        return died;
    }

    public void setDied(boolean died) {
        this.died = died;
    }

    /**
     * Bounds are approximate to a circle.
     * @return Circle
     */
    public Circle getCircleBounds() {
        return circleBounds;
    }

    public void setCircleBounds(Circle circleBounds) {
        this.circleBounds = circleBounds;
    }
    
    /**
     * Set position of circle bounds.
     * @param x
     * @param y 
     */
    public void setCirclePosition(double x, double y){
        this.circleBounds.setCenterX(x);
        this.circleBounds.setCenterY(y);
    }    
    
    @Override
    public void setPosition(double x, double y){
        this.setX(x);
        this.setY(y);
        setCirclePosition((this.getFw()/2)+x, y+this.getFh()-circleBounds.getRadius());
    }
}
