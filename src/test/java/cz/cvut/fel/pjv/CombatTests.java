/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.gameVariables;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.glh;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.spriteManager;
import static cz.cvut.fel.pjv.Util.HERO_DOWN;
import static cz.cvut.fel.pjv.Util.HERO_FRAMES;
import static cz.cvut.fel.pjv.Util.HERO_HEIGHT;
import static cz.cvut.fel.pjv.Util.HERO_IMAGE;
import static cz.cvut.fel.pjv.Util.HERO_STAND;
import static cz.cvut.fel.pjv.Util.HERO_WIDTH;
import static cz.cvut.fel.pjv.Util.RED_SLIME_IMAGE;
import static cz.cvut.fel.pjv.Util.SLIME_FRAMES;
import static cz.cvut.fel.pjv.Util.SLIME_HEIGHT;
import static cz.cvut.fel.pjv.Util.SLIME_JUMP;
import static cz.cvut.fel.pjv.Util.SLIME_WIDTH;
import static cz.cvut.fel.pjv.Util.SWORD_DOWN_X;
import static cz.cvut.fel.pjv.Util.SWORD_DOWN_Y;
import static cz.cvut.fel.pjv.Util.SWORD_HEIGHT;
import static cz.cvut.fel.pjv.Util.SWORD_IMAGE;
import static cz.cvut.fel.pjv.Util.SWORD_WIDTH;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Sofie
 */
/*
If i used junit5 there will be 
@Tag("Combat Tests")
*/
public class CombatTests {
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static Level level;
    static ObjectMapper objectMapper;
    static List<Sprite> inventory;
    static Random rand = new Random();
    
    public CombatTests() {
        
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        glh.resetVariables();
        spriteManager.clearAllSprites();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
     //1
    @Test
    public void testcheckAttackTrue(){
        System.out.println("kill slime");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(232, 200);
        spriteManager.addSprite(hero);
        Sword sword = new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 2*SWORD_WIDTH, 2*SWORD_HEIGHT, 0, false);
        sword.setHidden(false);
        sword.setPosition(hero.getX()-8, hero.getY()+2*HERO_HEIGHT-5);
        spriteManager.addSprite(sword);
        slime slime = new slime(SpriteId.SLIME_RED, RED_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2*SLIME_WIDTH, 2*SLIME_HEIGHT, SLIME_FRAMES, true);
        slime.setGroupName("SLIMES");
        spriteManager.addSprite(slime);
        slime.setPosition(200, 220);
        glh.checkAttack();
        assertTrue(slime.isDied());
    }
    
    //2
    @Test
    public void testcheckAttackFalse(){
        System.out.println("hero attacks, but nothing happenned - slime is too far");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        Sword sword = new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 2*SWORD_WIDTH, 2*SWORD_HEIGHT, 0, false);
        sword.setPosition(hero.getX()-8, hero.getY()+2*HERO_HEIGHT-5);
        sword.setHidden(false);
        spriteManager.addSprite(sword);
        slime slime = new slime(SpriteId.SLIME_RED, RED_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2*SLIME_WIDTH, 2*SLIME_HEIGHT, SLIME_FRAMES, true);
        slime.setGroupName("SLIMES");
        spriteManager.addSprite(slime);
        slime.setPosition(200, 250);
        glh.checkAttack();
        boolean result = slime.isDied();
        assertFalse(result);
    }
    
    //3
    @Test
    public void CheckAttackSlimeTrue(){
        System.out.println("slime hits the hero");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        Sword sword = new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 2*SWORD_WIDTH, 2*SWORD_HEIGHT, 0, false);
        spriteManager.addSprite(sword);
        slime slime = new slime(SpriteId.SLIME_RED, RED_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2*SLIME_WIDTH, 2*SLIME_HEIGHT, SLIME_FRAMES, true);
        slime.setGroupName("SLIMES");
        spriteManager.addSprite(slime);
        slime.setPosition(200, 180);
        glh.checkAttack();
        System.out.println(slime.isDied());
        int result = gameVariables.get("Health");
        assertEquals(4, result);
    }
    
    //4
    @Test
    public void CheckAttackSlimeFalse(){
        System.out.println("slime cant hit the hero because is dead");
        System.out.println("slime hits the hero");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        Sword sword = new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 2*SWORD_WIDTH, 2*SWORD_HEIGHT, 0, false);
        spriteManager.addSprite(sword);
        slime slime = new slime(SpriteId.SLIME_RED, RED_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2*SLIME_WIDTH, 2*SLIME_HEIGHT, SLIME_FRAMES, true);
        slime.setGroupName("SLIMES");
        spriteManager.addSprite(slime);
        slime.setPosition(200, 180);
        slime.setDied(true);
        glh.checkAttack();
        System.out.println(slime.isDied());
        int result = gameVariables.get("Health");
        assertEquals(5, result);
    }
}
