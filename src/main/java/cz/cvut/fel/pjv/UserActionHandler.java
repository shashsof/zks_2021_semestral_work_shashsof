
package cz.cvut.fel.pjv;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventType;


/**
 * 
 * @author sonia
 */
public class UserActionHandler implements EventHandler<KeyEvent> {
  private final SpriteManager spriteManager;
  private boolean sword_pressed = false;
  private GameLoopHandler gameLoopHandler;
 

  public UserActionHandler(SpriteManager spriteManager, GameLoopHandler gameLoopHandler) {
    this.spriteManager = spriteManager;
    this.gameLoopHandler = gameLoopHandler;
  }

  @Override
  public void handle(KeyEvent event) {
    if (event.getEventType() == KeyEvent.KEY_PRESSED) { 
      if (null != event.getCode()) switch (event.getCode()) {
            case UP:{
                Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
                hero.setSpeedY(-5);
                hero.setDirection("up");
                hero.setAnimate(true);
                    break;
                }
            case DOWN:{
                Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
                hero.setSpeedY(5);
                hero.setDirection("down");
                hero.setAnimate(true);
                    break;
                }
            case RIGHT:{
                Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
                hero.setSpeedX(5);
                hero.setDirection("right");
                hero.setAnimate(true);
                    break;
                }
            case LEFT:{
                Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
                hero.setSpeedX(-5);
                hero.setDirection("left");
                hero.setAnimate(true);
                    break;
                }
            case SPACE:{
                Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
                Sword sword = (Sword)spriteManager.getSprite(SpriteId.SWORD);
                if(!sword_pressed){
                    sword.setFh(2*SWORD_HEIGHT);
                    sword.setFw(2*SWORD_WIDTH);
                    sword.setHidden(false);
                    sword_pressed = true;
                }else{
                    sword.setFh(0);
                    sword.setFw(0);
                    sword.setHidden(true);
                }switch(hero.getDirection()){
                    case "down":
                        sword.setDirection("down");
                        break;
                    case "up":
                        sword.setDirection("up");
                        break;
                    case "right":
                        sword.setDirection("right");
                        break;
                    case "left":
                        sword.setDirection("left");
                        break;
                }   break;
            }case S:{
                try {
                    gameLoopHandler.saveGame();
                } catch (IOException ex) {
                    Logger.getLogger(UserActionHandler.class.getName()).log(Level.SEVERE, "Error in saving game. Game not saved.", ex);
                }
                break;
            }case L:{
                try {
                    gameLoopHandler.loadGame("savedGame.json");
                } catch (IOException ex) {
                    Logger.getLogger(UserActionHandler.class.getName()).log(Level.SEVERE, "Error in loading game. No such file exists.", ex);
                }
                break;
            }default:
                break;
        }
    }else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        Sword sword = (Sword)spriteManager.getSprite(SpriteId.SWORD);
        sword.setFh(0);
        sword.setFw(0);
        sword.setHidden(true); 
        hero.setSpeedY(0);
        hero.setSpeedX(0);
        hero.setAnimate(false);  
        if(event.getCode() == KeyCode.SPACE){
            sword_pressed = false;
        }
    }
    Logger.getLogger(UserActionHandler.class.getName()).log(java.util.logging.Level.INFO, "{0} {1}", new Object[]{String.valueOf(event.getEventType()), String.valueOf(event.getCode())});
  }
}
