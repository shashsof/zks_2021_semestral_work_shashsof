package cz.cvut.fel.pjv;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.TimelineBuilder;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;

import static cz.cvut.fel.pjv.Util.*;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MainApp extends Application {
  private GameLoopHandler gameLoopHandler;
  private SpriteManager spriteManager;
  private GameCanvas gameCanvas;
  private UserActionHandler userActionHandler;
  private Map<String, Integer> gameVariables;


  @Override
  public void init() throws Exception {
      // disable comment to rewrite levels files.
    //LevelWriter.CreateLevels();
    spriteManager = new SpriteManager();   
    gameVariables = new HashMap<>();
    gameCanvas = new GameCanvas(spriteManager, gameVariables);
    gameLoopHandler = new GameLoopHandler(gameCanvas, spriteManager, gameVariables);
    userActionHandler = new UserActionHandler(spriteManager, gameLoopHandler);
  }
  
  @Override
  public void start(Stage stage) throws Exception{
     
    //build the scene
    StackPane stackPane = new StackPane();
    stackPane.getChildren().add(gameCanvas);
    Scene scene = new Scene(stackPane, INIT_APP_WIDTH, INIT_APP_HEIGHT);
    stage.setScene(scene);
    stage.setTitle("RPG Game");
    
    //icon
    stage.getIcons().add(new Image(GFX_DIR + "open_tileset.png")); 
    
    // music (doesn't work on linux?)
    Media media = new Media(getClass().getResource("/music/Harp.mp3").toURI().toString());
    MediaPlayer musicplayer = new MediaPlayer(media);
    musicplayer.setAutoPlay(true);
    musicplayer.setVolume(0.9);   // from 0 to 1      
    
    musicplayer.setOnEndOfMedia(new Runnable() {     
        @Override
        public void run() {
            musicplayer.seek(Duration.ZERO); 
            musicplayer.play();
        }
    }); 
    
    
     //add mouse and keyboard event handling
    scene.setOnKeyPressed(userActionHandler);
    scene.setOnKeyReleased(userActionHandler);
    
    //create the game loop
    final KeyFrame oneFrame = new KeyFrame(
      Duration.millis(1000 / 60), // [60 fps]
      gameLoopHandler
    );
    TimelineBuilder.create()
      .cycleCount(Animation.INDEFINITE)
      .keyFrames(oneFrame)
      .build()
      .play();

    //show the stage
    stage.show();
  }

  /**
   * The main() method is ignored in correctly deployed JavaFX application.
   * main() serves only as fallback in case the application can not be launched
   * through deployment artifacts, e.g., in IDEs with limited FX support.
   * NetBeans ignores main().
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }
}
