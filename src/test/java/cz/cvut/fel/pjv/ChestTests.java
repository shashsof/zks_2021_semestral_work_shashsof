
package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.gameVariables;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.glh;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.spriteManager;
import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Sofie
 */
/*
If i used junit5 there will be 
@Tag("Chest Tests")
*/
public class ChestTests {
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static Level level;
    static ObjectMapper objectMapper;
    static List<Sprite> inventory;
    static Random rand = new Random();
    
    public ChestTests() {
        
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        glh.resetVariables();
        spriteManager.clearAllSprites();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    //1
    @Test
    public void testCheckPickingItem(){
        System.out.println("testing picking a key");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        Sprite key = new Sprite(SpriteId.GOLD_KEY, KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        key.setPosition(220, 220);
        key.setGroupName("ITEM");
        spriteManager.addSprite(key);
        glh.checkPickingItem();
        int expected = 1;
        int result = gameVariables.get("GOLD_KEY");
        assertEquals(expected, result);
    }
    
    //2
    @Test
    public void testOpenChestRight(){
        System.out.println("testing opening chest - succes");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        gameVariables.put("GOLD KEY", 1);
        Sprite key = new Sprite(SpriteId.GOLD_KEY, KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        key.setGroupName("ITEM");
        glh.putToInventory(key);
        Sprite chest = new Obstacle(SpriteId.CHEST1, OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
        chest.setPosition(220, 220);
        chest.setGroupName("CHEST");
        spriteManager.addSprite(chest);
        glh.openChest();
        int result = gameVariables.get("CHEST1");
        assertEquals(1, result);
    }
    
    //3
    @Test
    public void testOpenChestWrongPosition(){
        System.out.println("testing opening chest - fail - hero is too far away");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(100,100);
        spriteManager.addSprite(hero);
        gameVariables.put("GOLD KEY", 1);
        Sprite key = new Sprite(SpriteId.GOLD_KEY, KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        key.setGroupName("ITEM");
        glh.putToInventory(key);
        Sprite chest = new Obstacle(SpriteId.CHEST1, OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
        chest.setPosition(220, 220);
        chest.setGroupName("CHEST");
        spriteManager.addSprite(chest);
        glh.openChest();
        int result = gameVariables.get("CHEST1");
        assertEquals(0, result);
    }
    
    //4
    @Test
    public void testOpenChestHeroIsNull(){
        System.out.println("testing opening chest - fail - hero is null");
        gameVariables.put("GOLD KEY", 1);
        Sprite key = new Sprite(SpriteId.GOLD_KEY, KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        key.setGroupName("ITEM");
        glh.putToInventory(key);
        Sprite chest = new Obstacle(SpriteId.CHEST1, OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
        chest.setPosition(220, 220);
        chest.setGroupName("CHEST");
        spriteManager.addSprite(chest);
        thrown.expect(NullPointerException.class);
        glh.openChest();
    }
    
    //5
    @Test
    public void testOpenChestWrongKey(){
        System.out.println("testing opening chest - fail wrong key");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        gameVariables.put("SILVER_KEY", 1);
        Sprite key = new Sprite(SpriteId.SILVER_KEY, KEY_IMAGE, SILVER_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        key.setGroupName("ITEM");
        glh.putToInventory(key);
        Sprite chest = new Obstacle(SpriteId.CHEST1, OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
        chest.setPosition(220, 220);
        chest.setGroupName("CHEST");
        spriteManager.addSprite(chest);
        glh.openChest();
        int result = gameVariables.get("CHEST1");
        assertEquals(0, result);
    }
}
