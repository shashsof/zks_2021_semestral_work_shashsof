/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author sonia
 */
/*
If i used junit5 there will be 
@Tag("Game Loop Handler Tests")
*/
public class GameLoopHandlerTest {
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static Level level;
    static ObjectMapper objectMapper;
    static List<Sprite> inventory;
    static Random rand = new Random();
    
    public GameLoopHandlerTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        glh.resetVariables();
        spriteManager.clearAllSprites();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    //1
    @Test
    public void testLoadGameException1() throws Exception {
        System.out.println("load non existing file");
        thrown.expect(IOException.class);
        glh.loadGame("");   
    }
    
    //2
    @Test
    public void testLoadGameException2() throws Exception {
        System.out.println("load file but its not saved game");
        thrown.expect(Exception.class);
        glh.loadGame("test.json");   
    }
    
    //3
    @Test
    public void testLoadGameCorrect() throws Exception {
        System.out.println("load Game");
        glh.loadGame("testSavedGame.json");   
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        assertNotNull(hero);
    }
    
    
}
