package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import java.util.Map;

import static cz.cvut.fel.pjv.Util.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javafx.scene.shape.Circle;

/**
 *
 * @author Sofia
 */
public class GameLoopHandler implements EventHandler {

    private final GameCanvas gameCanvas;
    private final SpriteManager spriteManager;
    private Map<String, Integer> gameVariables;
    private Level level;
    private ObjectMapper objectMapper;
    private final Random rand = new Random();
    private boolean immune;
    private int immuneCnt = 0;
    private List<Sprite> inventory;

    public GameLoopHandler(SpriteManager spriteManager, Map<String, Integer> gameVariables) throws IOException {
        // for testing
        this.gameCanvas = null;
        this.spriteManager = spriteManager;
        this.gameVariables = gameVariables;
        this.inventory = new ArrayList<>();
        gameVariables.put("Gold", 0);
        gameVariables.put("GOLD_KEY", 0);
        gameVariables.put("GOLD_KEYused", 0);
        gameVariables.put("SILVER_KEY", 0);
        gameVariables.put("HEART_KEY", 0);
        gameVariables.put("WOODEN_KEY", 0);
        gameVariables.put("ItemPos", 0);
        gameVariables.put("Lvl", 1);
        gameVariables.put("Health", 5);
        gameVariables.put("CHEST1", 0); //if chest is opened or not
        gameVariables.put("CHEST2", 0);
        gameVariables.put("CHEST3", 0);
        gameVariables.put("CHEST4", 0);
    }

    /**
     * Creates the main loop for game. It controls movement, collisions and
     * variables.
     *
     * @param gameCanvas
     * @param spriteManager
     * @param gameVariables
     * @throws IOException
     */
    public GameLoopHandler(
            GameCanvas gameCanvas,
            SpriteManager spriteManager,
            Map gameVariables) throws IOException {

        this.gameCanvas = gameCanvas;
        this.spriteManager = spriteManager;
        this.gameVariables = gameVariables;
        this.inventory = new ArrayList<>();
        gameVariables.put("Gold", 0);
        gameVariables.put("GOLD_KEY", 0);
        gameVariables.put("GOLD_KEYused", 0);
        gameVariables.put("SILVER_KEY", 0);
        gameVariables.put("HEART_KEY", 0);
        gameVariables.put("WOODEN_KEY", 0);
        gameVariables.put("ItemPos", 0);
        gameVariables.put("Lvl", 1);
        gameVariables.put("Health", 5);
        gameVariables.put("CHEST1", 0); //if chest is opened or not
        gameVariables.put("CHEST2", 0);
        gameVariables.put("CHEST3", 0);
        gameVariables.put("CHEST4", 0);
        initSprites("level1.json");
    }

    /**
     * Initialization of sprites. Uploading sprites to sprite manager with their
     * names and initial positions.
     *
     * @throws IOException
     */
    public void initSprites(String fname) throws IOException {
        // read the level from json file
        spriteManager.clearAllSprites();
        objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        level = objectMapper.readValue(new File(fname), Level.class);
        if (level.getGameVariables() != null && gameCanvas != null) {
            // if game is loaded, update game variables.
            gameVariables = level.getGameVariables();
            gameCanvas.setGameVariables(gameVariables);
        }
        if(level.getInventory() != null){
            if(!level.getInventory().isEmpty()){
                inventory.clear();
                gameVariables.put("ItemPos", 0);
                Sprite sp;
                for(Sprite item : level.getInventory()){
                    sp = new Sprite(item.getId(), item.getImageName(), item.getSx(), item.getSy(), item.getWidth(), item.getHeight(), item.getFw(), item.getFh(), item.getFrames(), item.isAnimate());
                    sp.setGroupName(item.getGroupName());
                    putToInventory(sp);
                }
            }  
        }
        
        
        Sprite sp = null;
        // Put sprites to a sprite Manager.
        for (Item item : level.level) {
            // Load sprites that is included in this level.
            switch (item.getGroupName()) {
                case "HERO":
                    sp = new Hero(item.getId(), HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2 * HERO_WIDTH, 2 * HERO_HEIGHT, HERO_FRAMES, false);
                    spriteManager.addSprite(sp);
                    break;
                case "SLIMES":
                    switch (item.getId()) {
                        case SLIME_RED:
                            sp = new slime(item.getId(), RED_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2 * SLIME_WIDTH, 2 * SLIME_HEIGHT, SLIME_FRAMES, true);
                            break;
                        case SLIME_BLUE:
                            sp = new slime(item.getId(), BLUE_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2 * SLIME_WIDTH, 2 * SLIME_HEIGHT, SLIME_FRAMES, true);
                            break;
                        case SLIME_GREEN:
                            sp = new slime(item.getId(), GREEN_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2 * SLIME_WIDTH, 2 * SLIME_HEIGHT, SLIME_FRAMES, true);
                            break;
                        case SLIME_YELLOW:
                            sp = new slime(item.getId(), YELLOW_SLIME_IMAGE, 0, SLIME_JUMP, SLIME_WIDTH, SLIME_HEIGHT, 2 * SLIME_WIDTH, 2 * SLIME_HEIGHT, SLIME_FRAMES, true);
                            break;
                    }
                    break;
                case "ROCKS":
                    sp = new Obstacle(item.getId(), OBSTACLE_IMAGE, ROCK_X, ROCK_Y, ROCK_WIDTH, ROCK_HEIGHT, 2 * ROCK_WIDTH, 2 * ROCK_HEIGHT, 0, false);
                    break;
                case "BUSHES":
                    sp = new Obstacle(item.getId(), OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
                    break;
                case "CHEST":
                    if (gameVariables.get(String.valueOf(item.getId())) == 0) {
                        sp = new Obstacle(item.getId(), OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
                    } else {
                        sp = new Obstacle(item.getId(), OBSTACLE_IMAGE, CHEST_OPENED_X, CHEST_OPENED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
                    }
                    break;
                case "ITEM":
                    switch (item.getId()) {
                        case GOLD_KEY:
                            sp = new Sprite(item.getId(), KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, 0, 0, 0, false);
                            break;
                        case SILVER_KEY:
                            sp = new Sprite(item.getId(), KEY_IMAGE, SILVER_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, 0, 0, 0, false);
                            break;
                        case WOODEN_KEY:
                            sp = new Sprite(item.getId(), KEY_IMAGE, WOODEN_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, 0, 0, 0, false);
                            break;
                        case HEART_KEY:
                            sp = new Sprite(item.getId(), KEY_IMAGE, HEART_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, 0, 0, 0, false);
                            break;
                        case POTION:
                            sp = new Sprite(item.getId(), POTION_IMAGE, POTION_X, POTION_Y, POTION_WIDTH, POTION_HEIGHT, 0, 0, 0, false);
                            break;
                    }
                    break;
            }
            spriteManager.addSprite(sp);
            sp.setGroupName(item.getGroupName());
        }
        // Add sword. Sword is in every level and it's hidden on the start.
        spriteManager.addSprite(new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 0, 0, 0, false));

        // Put sprites to their initial positions.
        for (Item item : level.level) {
            sp = spriteManager.getSprite(item.getId());
            if (sp.getGroupName().equals("HERO")) {
                Hero hero = (Hero) sp;
                hero.setSpeedX(0);
                hero.setSpeedY(0);
            } else if (sp.getGroupName().equals("SLIMES")) {
                slime slime = (slime) sp;
                slime.setSpeedX(0);
                slime.setSpeedY(0);
            } else if (sp.getGroupName().equals("ITEM")){
                if (gameVariables.get(String.valueOf(item.getId())+"used") == 0 &&
                        gameVariables.get(String.valueOf(item.getId()))==0){
                    sp.setFh(ITEM_WIDTH);
                    sp.setFw(ITEM_HEIGHT);
                }
            }
            sp.setPosition(item.getX(), item.getY());
        }
        if(!inventory.isEmpty() && level.getInventory() == null){
            gameVariables.put("ItemPos", 0);
            for (Sprite item : inventory){
                sp = new Sprite(item.getId(), item.getImageName(), item.getSx(), item.getSy(), item.getWidth(), item.getHeight(), item.getFw(), item.getFh(), item.getFrames(), item.isAnimate());
                sp.setGroupName(item.getGroupName());
                spriteManager.addSprite(sp);
                gameVariables.put("ItemPos", gameVariables.get("ItemPos") + (int) item.getWidth());
                sp.setPosition(INIT_APP_WIDTH - gameVariables.get("ItemPos"), INIT_APP_HEIGHT - item.getHeight()); 
            }
        }
    }

    /**
     * Update coordinates of the sprites based on their movement, colisions with
     * obstacles and player actions.
     */
    public void updateSprites() {
        // hero movement
        Hero hero = (Hero) spriteManager.getSprite(SpriteId.HERO);
        //move if not hit obstacle
        if (!checkObstacle(hero)) {
            hero.increaseX(hero.getSpeedX());
            hero.increaseY(hero.getSpeedY());
        }
        // appereance of sprite based on direction
        if (hero.getDirection().equalsIgnoreCase("up")) {
            hero.setSy(HERO_UP);
        } else if (hero.getDirection().equalsIgnoreCase("down")) {
            hero.setSy(HERO_DOWN);
        } else if (hero.getDirection().equalsIgnoreCase("left")) {
            hero.setSy(HERO_LEFT);
        } else if (hero.getDirection().equalsIgnoreCase("right")) {
            hero.setSy(HERO_RIGHT);
        }
        // chosing apereance based if hero is moving or not
        if (hero.isAnimate()) {
            hero.setSx(HERO_WALK);
        } else {
            hero.setSx(HERO_STAND);
        }

        Logger.getLogger(GameLoopHandler.class.getName()).log(java.util.logging.Level.INFO, "Hero coordinates x:{0} y: {1}", new Object[]{hero.getX(), hero.getY()});

        // slime movement
        for (Sprite sprite : spriteManager.getSprites()) {
            double x;
            double y;
            if (sprite.getGroupName().equals("SLIMES")) {
                slime slime = (slime) sprite;
                // if slime is died, stop the movement and change to dying animation. 
                if (slime.isDied()) {
                    slime.setSpeedX(0);
                    slime.setSpeedY(0);
                    slime.setSy(SLIME_DIE);
                    if (slime.getCnt() == slime.getFrames() - 1) {
                        // spawn on random place, if it collides with obstacle 
                        // change to the next random position.
                        do {
                            x = Math.abs(rand.nextInt(1280 - 64));
                            y = Math.abs(rand.nextInt(640 - 64));
                            slime.setPosition(x, y);
                        } while (checkObstacle(slime));
                        slime.setDied(false);
                        slime.setSy(SLIME_JUMP);
                    }
                } else {
                    // choose random direction to move.
                    if (slime.getSpeedX() == 0 && slime.getSpeedY() == 0) {
                        int c = rand.nextInt(4);
                        switch (c) {
                            case 0:
                                slime.setSpeedX(1);
                                break;
                            case 1:
                                slime.setSpeedX(-1);
                                break;
                            case 2:
                                slime.setSpeedY(1);
                                break;
                            case 3:
                                slime.setSpeedY(-1);
                                break;
                        }
                    } else if (slime.getCnt() == slime.getFrames() - 1) {
                        // if its end of animation stop the movement
                        slime.setSpeedX(0);
                        slime.setSpeedY(0);
                    }
                    if (!checkObstacle(slime)) {
                        // change position if there is no obstacle.
                        slime.setX(slime.getX() + slime.getSpeedX());
                        slime.setY(slime.getY() + slime.getSpeedY());
                        slime.setCirclePosition((slime.getFw() / 2) + slime.getX(), slime.getY() + slime.getFh() - slime.getCircleBounds().getRadius());
                    }
                }
            }
        }

        //sword appearence
        Sword sword = (Sword) spriteManager.getSprite(SpriteId.SWORD);
        if (sword.isHidden()) {
            sword.setPosition(-100, -100);
        } else {
            switch (sword.getDirection()) {
                case "down":
                    sword.setSx(SWORD_DOWN_X);
                    sword.setSy(SWORD_DOWN_Y);
                    sword.setPosition(hero.getX() - 8, hero.getY() + 2 * HERO_HEIGHT - 5);
                    break;
                case "up":
                    sword.setSx(SWORD_UP_X);
                    sword.setSy(SWORD_UP_Y);
                    sword.setPosition(hero.getX() + 10, hero.getY() - HERO_HEIGHT + 10);
                    break;
                case "right":
                    sword.setSx(SWORD_RIGHT_X);
                    sword.setSy(SWORD_RIGHT_Y);
                    sword.setPosition(hero.getX() + HERO_WIDTH, hero.getY() + 10);
                    break;
                case "left":
                    sword.setSx(SWORD_LEFT_X);
                    sword.setSy(SWORD_LEFT_Y);
                    sword.setPosition(hero.getX() - HERO_WIDTH, hero.getY() + 10);
                    break;
            }
        }
    }

    /**
     * prevent sprites to go out of screen.
     */
    public void endOfScreen() {
        for (Sprite sprite : spriteManager.getSprites()) {
            if (sprite.getX() > INIT_APP_WIDTH - sprite.getFw()) {
                sprite.setX(INIT_APP_WIDTH - sprite.getFw());
            } else if (sprite.getX() < 0) {
                sprite.setX(0);
            }
            if (sprite.getY() > INIT_APP_HEIGHT - sprite.getFh()) {
                sprite.setY(INIT_APP_HEIGHT - sprite.getFh());
            } else if (sprite.getY() < 0) {
                sprite.setY(0);
            }
        }
    }

    /**
     * Checking if sword hits the enemy or if slime hits the hero.
     */
    public void checkAttack() {
        double swordDistanceX = 0; // distance of centers of objects on x axis
        double swordDistanceY = 0; // distance of centers of objects on y axis
        double swordDistance; // pythagorian distance of objects
        double heroDistanceX = 0; // distance of centers of objects on x axis
        double heroDistanceY = 0; // distance of centers of objects on y axis
        double heroDistance; // pythagorian distance of objects

        Rectangle2D swordBounds, heroBounds;
        Circle slimeBounds;
        Sword sword = (Sword) spriteManager.getSprite(SpriteId.SWORD);
        swordBounds = sword.getBounds();
        Hero hero = (Hero) spriteManager.getSprite(SpriteId.HERO);
        heroBounds = hero.getBounds();
        immuneCnt++;
        if (immuneCnt == 60) {
            immuneCnt = 0;
            immune = false;
        }
        for (Sprite sprite : spriteManager.getSprites()) {
            if (sprite.getGroupName().equals("SLIMES")) {

                slime slime = (slime) sprite;
                slimeBounds = slime.getCircleBounds();
                for (int i = 0; i < 4; i++) {
                    if (i == 0) {
                        // upper-left corner
                        heroDistanceX = heroBounds.getMinX() - slimeBounds.getCenterX();
                        heroDistanceY = heroBounds.getMinY() - slimeBounds.getCenterY();
                        swordDistanceX = swordBounds.getMinX() - slimeBounds.getCenterX();
                        swordDistanceY = swordBounds.getMinY() - slimeBounds.getCenterY();
                    } else if (i == 1) {
                        // lower-left corner
                        heroDistanceX = heroBounds.getMinX() - slimeBounds.getCenterX();
                        heroDistanceY = heroBounds.getMaxY() - slimeBounds.getCenterY();
                        swordDistanceX = swordBounds.getMinX() - slimeBounds.getCenterX();
                        swordDistanceY = swordBounds.getMaxY() - slimeBounds.getCenterY();
                    } else if (i == 2) {
                        // upper-right corner
                        heroDistanceX = heroBounds.getMaxX() - slimeBounds.getCenterX();
                        heroDistanceY = heroBounds.getMinY() - slimeBounds.getCenterY();
                        swordDistanceX = swordBounds.getMaxX() - slimeBounds.getCenterX();
                        swordDistanceY = swordBounds.getMinY() - slimeBounds.getCenterY();
                    } else if (i == 3) {
                        // lower-right corner
                        heroDistanceX = heroBounds.getMaxX() - slimeBounds.getCenterX();
                        heroDistanceY = heroBounds.getMaxY() - slimeBounds.getCenterY();
                        swordDistanceX = swordBounds.getMaxX() - slimeBounds.getCenterX();
                        swordDistanceY = swordBounds.getMaxY() - slimeBounds.getCenterY();
                    }
                    swordDistance = Math.sqrt(swordDistanceX * swordDistanceX + swordDistanceY * swordDistanceY);
                    heroDistance = Math.sqrt(heroDistanceX * heroDistanceX + heroDistanceY * heroDistanceY);
                    if (swordDistance < slimeBounds.getRadius() && !slime.isDied()) {
                        gameVariables.put("Gold", gameVariables.get("Gold") + 1);
                        slime.setDied(true);
                        slime.setCnt(0);
                    }
                    if (heroDistance < slimeBounds.getRadius() && !immune && !slime.isDied()) {
                        immune = true;
                        gameVariables.put("Health", gameVariables.get("Health") - 1);
                        if (Math.abs(heroDistanceX) >= Math.abs(heroDistanceY)) {
                            if (heroDistanceX >= 0) {
                                hero.setX(hero.getX() + 25);
                            } else {
                                hero.setX(hero.getX() - 25);
                            }
                        } else {
                            if (heroDistanceY >= 0) {
                                hero.setY(hero.getY() + 25);
                            } else {
                                hero.setY(hero.getY() - 25);
                            }
                        }
                    }
                }

            }
        }
    }

    /**
     * Controlling if sprite is colliding with obstacle. If so returns true, and
     * move this sprite to oposite direction.
     *
     * @param sp
     * @return true if sprite is colliding with obstaclw and false if not
     */
    public boolean checkObstacle(Sprite sp) {
        Rectangle2D borders = sp.getBounds();
        Rectangle2D obstacle;
        for (Sprite sprite : spriteManager.getSprites()) {
            if (sprite.getGroupName().equals("ROCKS") || sprite.getGroupName().equals("BUSHES")
                    || sprite.getGroupName().equals("CHEST")) {
                obstacle = sprite.getBounds();
                if (obstacle.intersects(borders)) {
                    if (obstacle.getMaxX() > borders.getMinX() && obstacle.getMaxX() - 6 < borders.getMinX()) {
                        sp.setX(sp.getX() + 1);
                    } else if (obstacle.getMinX() + 6 > borders.getMaxX() && obstacle.getMinX() < borders.getMaxX()) {
                        sp.setX(sp.getX() - 1);
                    } else if (obstacle.getMaxY() > borders.getMinY() && obstacle.getMaxY() - 6 < borders.getMinY()) {
                        sp.setY(sp.getY() + 1);
                    } else if (obstacle.getMinY() + 6 > borders.getMaxY() && obstacle.getMinY() < borders.getMaxY()) {
                        sp.setY(sp.getY() - 1);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checking if hero picks particular item. If so move this item to
     * bottom-right corner of the screen (inventory) and update game variables.
     */
    public void checkPickingItem() {
        Hero hero = (Hero) spriteManager.getSprite(SpriteId.HERO);
        for (Sprite item : spriteManager.getSprites()) {
            if (item.getGroupName().equals("ITEM")) {
                if (hero.getBounds().intersects(item.getBounds()) && gameVariables.get(String.valueOf(item.getId())) == 0) {
                    putToInventory(item);
                }
            }
        }
    }

    /**
     * put item to inventory.
     * @param item Sprite.
     */
    public void putToInventory(Sprite item) {
        gameVariables.put(String.valueOf(item.getId()), 1);
        gameVariables.put("ItemPos", gameVariables.get("ItemPos") + (int) item.getWidth());
        item.setPosition(INIT_APP_WIDTH - gameVariables.get("ItemPos"), INIT_APP_HEIGHT - item.getHeight());
        inventory.add(item);
        spriteManager.addSprite(item);
    }

    /**
     * remove item from inventory.
     * @param item Sprite.
     */
    public void removeFromInventory(Sprite item) {
        gameVariables.put(String.valueOf(item.getId()), 0);
        gameVariables.put("ItemPos", gameVariables.get("ItemPos") - (int) item.getWidth());
        item.setFh(0);
        item.setFw(0);
        inventory.remove(item);
        spriteManager.deleteSprite(item.getId());
    }

    public void saveGame() throws IOException {
        SavedGame savedGame = new SavedGame(gameVariables, level, spriteManager, inventory);
        savedGame.saveGame("savedGame.json");
    }
    
    public void saveGame(String path) throws IOException {
        SavedGame savedGame = new SavedGame(gameVariables, level, spriteManager, inventory);
        savedGame.saveGame(path);
    }

    public void loadGame(String fname) throws IOException {
        initSprites(fname);
    }

    /**
     * Cheking if some chest is opened and what will happen.
     */
    public void openChest() {
        Sprite item;
        Hero hero = (Hero) spriteManager.getSprite(SpriteId.HERO);
        for (Sprite sprite : spriteManager.getSprites()) {
            if (sprite.getGroupName().equals("CHEST") && hero.getBounds().intersects(sprite.getBounds())) {
                switch (sprite.getId()) {
                    case CHEST1:
                        if (gameVariables.get("GOLD_KEY") == 1) {
                            gameVariables.put("CHEST1", 1);
                            gameVariables.put("GOLD_KEYused", 1);
                            item = spriteManager.getSprite(SpriteId.GOLD_KEY);
                            removeFromInventory(item);
                            sprite.setSx(CHEST_OPENED_X);
                            sprite.setSy(CHEST_OPENED_Y);
                            item = new Sprite(SpriteId.SILVER_KEY, KEY_IMAGE, SILVER_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
                            item.setGroupName("ITEM");
                            putToInventory(item);
                        }
                        break;
                    case CHEST2:
                        if (gameVariables.get("SILVER_KEY") == 1) {
                            gameVariables.put("CHEST2", 1);
                            gameVariables.put("SILVER_KEYused", 1);
                            item = spriteManager.getSprite(SpriteId.SILVER_KEY);
                            removeFromInventory(item);
                            sprite.setSx(CHEST_OPENED_X);
                            sprite.setSy(CHEST_OPENED_Y);
                            item = new Sprite(SpriteId.WOODEN_KEY, KEY_IMAGE, WOODEN_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
                            item.setGroupName("ITEM");
                            putToInventory(item);
                            item = new Sprite(SpriteId.HEART_KEY, KEY_IMAGE, HEART_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
                            item.setGroupName("ITEM");
                            putToInventory(item);
                        }
                        break;
                    case CHEST3:
                        if (gameVariables.get("WOODEN_KEY") == 1) {
                            gameVariables.put("CHEST3", 1);
                            gameVariables.put("WOODEN_KEYused", 1);
                            item = spriteManager.getSprite(SpriteId.WOODEN_KEY);
                            removeFromInventory(item);
                            sprite.setSx(CHEST_OPENED_X);
                            sprite.setSy(CHEST_OPENED_Y);
                            gameVariables.put("Gold", gameVariables.get("Gold") + 100);
                        }
                        break;
                    case CHEST4:
                        if (gameVariables.get("HEART_KEY") == 1) {
                            gameVariables.put("CHEST4", 1);
                            gameVariables.put("HEART_KEYused", 1);
                            item = spriteManager.getSprite(SpriteId.HEART_KEY);
                            removeFromInventory(item);
                            sprite.setSx(CHEST_OPENED_X);
                            sprite.setSy(CHEST_OPENED_Y);
                            level.setDone(true);
                        }
                        break;
                }
            }
        }
    }

    
    /**
     * reset game.
     */
    public void resetVariables() {
        gameVariables.clear();
        gameVariables.put("Gold", 0);
        gameVariables.put("GOLD_KEY", 0);
        gameVariables.put("GOLD_KEYused", 0);
        gameVariables.put("SILVER_KEY", 0);
        gameVariables.put("HEART_KEY", 0);
        gameVariables.put("WOODEN_KEY", 0);
        gameVariables.put("ItemPos", 0);
        gameVariables.put("Lvl", 1);
        gameVariables.put("Health", 5);
        gameVariables.put("CHEST1", 0); //if chest is opened or not
        gameVariables.put("CHEST2", 0);
        gameVariables.put("CHEST3", 0);
        gameVariables.put("CHEST4", 0);
        inventory.clear();
    }

    @Override
    public void handle(Event event) {
        updateSprites();
        gameCanvas.redraw();
        endOfScreen();
        gameCanvas.redraw();
        checkPickingItem();
        gameCanvas.redraw();
        checkAttack();
        gameCanvas.redraw();
        openChest();
        gameCanvas.redraw();
        if (level.isDone()) {
            gameVariables.put("Lvl", gameVariables.get("Lvl") + 1);
            try {
                initSprites("level" + gameVariables.get("Lvl") + ".json");
            } catch (IOException ex) {
                Logger.getLogger(GameLoopHandler.class.getName()).log(java.util.logging.Level.SEVERE, "ERROR in loading level", ex);
            }
            gameCanvas.redraw();
        }
        if (gameVariables.get("Health") == 0) {
            resetVariables();
            try {
                initSprites("Level1.json");
            } catch (IOException ex) {
                Logger.getLogger(GameLoopHandler.class.getName()).log(java.util.logging.Level.SEVERE, "ERROR in loading level", ex);
            }
        }
    }

}
