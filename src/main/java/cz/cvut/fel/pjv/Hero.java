package cz.cvut.fel.pjv;

/**
 *
 * @author sonia
 */
public class Hero extends Sprite {
    private double speedY;
    private double speedX;
    private String direction;
    
    /**
     *
     * @param id
     * @param imageName image file
     * @param sourcex x coordinate in source image 
     * @param sourcey y coordinate in source image 
     * @param source_width width in source image
     * @param source_height height in source image
     * @param final_width displayed width
     * @param final_height displayed
     * @param frames how much frames are in animation
     * @param anim play animation or not
     */
    public Hero(SpriteId id, String imageName, 
            double sourcex, 
            double sourcey, 
            double source_width,
            double source_height,
            double final_width,
            double final_height,
            int frames,
            boolean anim) {
    super(id, imageName, sourcex, sourcey, source_width, source_height, final_width, final_height, frames, anim);
    speedY = 0;
    speedX = 0;
    direction = "down";
  }

    public double getSpeedY() {
        return speedY;
    }

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
    
    
    
}
