package cz.cvut.fel.pjv;

import java.util.Map;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.geometry.VPos;
import javafx.scene.text.TextAlignment;

import static cz.cvut.fel.pjv.Util.*;
import javafx.scene.image.Image;

/**
 *
 * @author Sofia
 */
public class GameCanvas extends Canvas {
  private final SpriteManager spriteManager;
  private Map<String, Integer> gameVariables;
  private final Image bg = new Image(BG_IMG);
  private final Image heart = new Image(HEART_IMAGE);

    /**
     * Initinalization of game canvas
     * @param spriteManager
     * @param gameVariables Map<String, integer>
     */
    public GameCanvas(SpriteManager spriteManager, Map gameVariables) {
    this.spriteManager = spriteManager;
    this.gameVariables = gameVariables;
    setHeight(INIT_APP_HEIGHT);
    setWidth(INIT_APP_WIDTH);
  }

    /**
     * Update sprites, background and gold on game screen.
     */
    public void redraw() {
    GraphicsContext gc = getGraphicsContext2D();
    renderBackground(gc);
    for (Sprite sprite : spriteManager.getSprites()) {
        sprite.render(gc);
    }
    renderGold(gc);
    renderHealth(gc);
  }
 
    /**
     * Clear the screen and then draws background image.
     */
  private void renderBackground(GraphicsContext gc) {
    double width = getWidth();
    double height = getHeight();
    gc.clearRect(0, 0, width, height);
    gc.drawImage(bg, 0, 0);
  }

    /**
     * Redraw gold number
     */
 private void renderGold(GraphicsContext gc) {
    String infoText = "Gold: " + gameVariables.get("Gold");
    gc.setFont(new Font(Font.getDefault().getName(), 50));
    gc.setTextAlign(TextAlignment.LEFT);
    gc.setTextBaseline(VPos.TOP);
    gc.fillText(infoText, 0, 0, INIT_APP_WIDTH);
  }

 private void renderHealth(GraphicsContext gc){
     for (int i = 1; i <= gameVariables.get("Health"); i++) {
         gc.drawImage(heart, HEART_X, HEART_Y, HEART_WIDTH, HEART_HEIGHT, INIT_APP_WIDTH-i*2*HEART_WIDTH, 0, 2*HEART_WIDTH, 2*HEART_HEIGHT);     
     }
 }
 /**
  * update game variables
  * @param gameVariables 
  */
    public void setGameVariables(Map<String, Integer> gameVariables) {
        this.gameVariables = gameVariables;
    } 
}
