package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static cz.cvut.fel.pjv.Util.*;
import java.util.Map;

/**
 * 
 * @author sonia
 */
class Item {
  private SpriteId id;
  private String groupName;
  private double x;
  private double y;

  public Item() {
  }

  /**
   * 
   * @param id SpriteId
   * @param groupName String
   * @param x double. initial x coordinates.
   * @param y double. initial y coordinates.
   */
  public Item(SpriteId id, String groupName, double x, double y) {
      this.id = id;
      this.groupName = groupName;
      this.x = x;
      this.y = y;
  }

  public SpriteId getId() {
        return id;
    }

  public String getGroupName() {
        return groupName;
    }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", groupName=" + groupName + ", x=" + x + ", y=" + y + '}';
    }
}

/**
 * 
 * @author sonia
 */
class Level {
  public List<Item> level = new ArrayList<>();
  private boolean done = false;
  private Map<String, Integer> gameVariables;
  private List<Sprite> inventory;
  
    public Level() {
        this.done = false;
    }

    public List<Sprite> getInventory() {
        return inventory;
    }

    public void setInventory(List<Sprite> inventory) {
        this.inventory = inventory;
    }
    
    public Map<String, Integer> getGameVariables() {
        return gameVariables;
    }

    public void setGameVariables(Map<String, Integer> gameVariables) {
        this.gameVariables = gameVariables;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
 
    /**
     * Add item to a level.
     * @param id SpriteId
     * @param groupName String
     * @param x initial x coordinates.
     * @param y initial y coordinates.
     */
    public void addItem(SpriteId id, String groupName, double x, double y) {
      level.add(new Item(id, groupName, x, y));
    }

    /**
     * Replace the initial coordinates of particular Sprite.
     * @param id
     * @param groupName
     * @param x
     * @param y 
     */
    // Used in saving game.
    public void replaceItem(SpriteId id, String groupName, double x, double y){
        int i = 0;
        for (Item item : level) {
          if(item.getId().equals(id)){
              level.set(i, new Item(id, groupName, x, y));
          }
          i++; 
        }
    }

  @Override
  public String toString() {
    return "GameBoard{" + "level=" + level + '}';
  }
  
}

class LevelWriter{
    /**
     * write levels to json files.
     * @throws IOException 
     */
    public static void CreateLevels() throws IOException {
        // write
        ObjectMapper objectMapper = new ObjectMapper();
        Level level1 = new Level();
        level1.addItem(SpriteId.HERO, "HERO", HERO_INIT_X, HERO_INIT_Y);
        level1.addItem(SpriteId.SLIME_RED, "SLIMES", 60, 32);
        level1.addItem(SpriteId.SLIME_BLUE, "SLIMES", 32, 640-64);
        level1.addItem(SpriteId.SLIME_YELLOW, "SLIMES", 1280-64, 32);
        level1.addItem(SpriteId.SLIME_GREEN, "SLIMES", 1280-64, 640-64);
        level1.addItem(SpriteId.BUSH1, "BUSHES", 0, 597);
        level1.addItem(SpriteId.BUSH2, "BUSHES", 150, 213);
        level1.addItem(SpriteId.BUSH4, "BUSHES", 1280-16, 448);
        level1.addItem(SpriteId.BUSH5, "BUSHES", 322, 599);
        level1.addItem(SpriteId.BUSH6, "BUSHES", 580, 52);
        level1.addItem(SpriteId.BUSH7, "BUSHES", 500, 107);
        level1.addItem(SpriteId.BUSH8, "BUSHES", 658, 284);
        level1.addItem(SpriteId.ROCK1, "ROCKS", 193, 267);
        level1.addItem(SpriteId.ROCK2, "ROCKS", 225, 335);
        level1.addItem(SpriteId.ROCK3, "ROCKS", 392, 175);
        level1.addItem(SpriteId.ROCK4, "ROCKS", 195, 346);
        level1.addItem(SpriteId.ROCK5, "ROCKS", 802, 220);
        level1.addItem(SpriteId.ROCK6, "ROCKS", 1224, 418);
        level1.addItem(SpriteId.ROCK7, "ROCKS", 25, 81);
        level1.addItem(SpriteId.ROCK8, "ROCKS", 423, 102);
        level1.addItem(SpriteId.GOLD_KEY, "ITEM", 1280-32, 320);
        level1.addItem(SpriteId.CHEST1, "CHEST", 0, 320);
        level1.addItem(SpriteId.CHEST2, "CHEST", 600, 600);
        level1.addItem(SpriteId.CHEST3, "CHEST", 600, 0);
        level1.addItem(SpriteId.CHEST4, "CHEST", 1150, 600);
        
        objectMapper.writeValue(new File("level1.json"), level1);
        
        
        Level level2 = new Level();
        level2.addItem(SpriteId.HERO, "HERO", HERO_INIT_X, HERO_INIT_Y);
        level2.addItem(SpriteId.SLIME_RED, "SLIMES", 60, 32);
        level2.addItem(SpriteId.SLIME_BLUE, "SLIMES", 32, 640-64);
        
        objectMapper.writeValue(new File("level2.json"), level2);
      }
}


   
