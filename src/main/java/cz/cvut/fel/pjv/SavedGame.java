package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sonia
 */
public class SavedGame {
    private Map<String, Integer> gameVariables;
    private Level level;
    private SpriteManager spriteManager;
    private List<Sprite> inventory;

    public SavedGame() {
    } 

    public SavedGame(Map<String, Integer> gameVariables, Level level, SpriteManager spriteManager, List<Sprite> inventory){
        this.gameVariables = gameVariables;
        this.level = level;
        this.spriteManager = spriteManager;
        this.inventory = inventory;
    }
    
    /**
     * Save game variables and positions of all sprites, and write them to json file. 
     * @throws IOException 
     */
    public void saveGame(String path) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        for(Sprite sprite : spriteManager.getSprites()){    
            level.replaceItem(sprite.getId(), sprite.getGroupName(), sprite.getX(), sprite.getY());
        }
        level.setGameVariables(gameVariables);
        level.setInventory(inventory);
        objectMapper.writeValue(new File(path), level);
    }
}
