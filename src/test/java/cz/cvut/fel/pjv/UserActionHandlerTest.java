package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.*;
import static cz.cvut.fel.pjv.MovingAndCollisionsTests.spriteManager;
import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sonia
 */
/*
If i used junit5 there will be 
@Tag("User Action Handler Tests")
*/
public class UserActionHandlerTest {
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static UserActionHandler uah;
    
    public UserActionHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
        uah = new UserActionHandler(spriteManager, glh);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws IOException {
        glh.initSprites("level1.json");
    }
    
    @After
    public void tearDown() {
    }

    //1
    @Test
    public void testArrow() {
        System.out.println("arrow pressed");
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        int expected = (int) (hero.getY() - 5);
        KeyEvent key = new KeyEvent(KeyEvent.KEY_PRESSED, "UP", "UP", KeyCode.UP, false, false, false, false);
        uah.handle(key);
        glh.updateSprites();
        int result = (int)hero.getY();
        assertEquals(expected, result);
    }
    
    //2
    @Test
    public void testSpacePress() {
        System.out.println("space pressed");
        Sword sword = (Sword)spriteManager.getSprite(SpriteId.SWORD);
        KeyEvent key = new KeyEvent(KeyEvent.KEY_PRESSED, "SPACE", "SPACE", KeyCode.SPACE, false, false, false, false);
        uah.handle(key);
        glh.updateSprites();
        assertFalse(sword.isHidden());
    }
    
    //3
    @Test
    public void testSpaceRelease(){
        System.out.println("space released");
        Sword sword = (Sword)spriteManager.getSprite(SpriteId.SWORD);
        KeyEvent key = new KeyEvent(KeyEvent.KEY_RELEASED, "SPACE", "SPACE", KeyCode.SPACE, false, false, false, false);
        uah.handle(key);
        glh.updateSprites();
        assertTrue(sword.isHidden());
    }
    
}
