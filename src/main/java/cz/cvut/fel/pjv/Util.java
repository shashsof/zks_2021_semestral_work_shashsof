package cz.cvut.fel.pjv;

/**
 *
 * @author Sofia
 */
public class Util {

  static final String GFX_DIR = "";

  static final double INIT_APP_WIDTH = 1280;
  static final double INIT_APP_HEIGHT = 640;
  

  // hero
  static final double HERO_INIT_X = INIT_APP_WIDTH/2;
  static final double HERO_INIT_Y = INIT_APP_HEIGHT / 2;
  
  static final double HERO_WIDTH = 16;
  static final double HERO_HEIGHT = 18;
  static final double HERO_UP = 0;
  static final double HERO_RIGHT = HERO_HEIGHT;
  static final double HERO_DOWN = 2*HERO_HEIGHT;
  static final double HERO_LEFT = 3*HERO_HEIGHT;
  static final double HERO_STAND = 0;
  static final double HERO_WALK = HERO_WIDTH;
  static final String HERO_IMAGE = "ninja.png";
  static final int HERO_FRAMES = 2;
  
  // Health
  static final double HEART_WIDTH = 16;
  static final double HEART_HEIGHT = 16;
  static final double HEART_X = 9*HEART_WIDTH;
  static final double HEART_Y = 6*HEART_HEIGHT;
  static final String HEART_IMAGE = "open_tileset.png";
  
  
  // Sword
  static final double SWORD_HEIGHT = 16;
  static final double SWORD_WIDTH = 16;
  static final double SWORD_DOWN_X = SWORD_WIDTH;
  static final double SWORD_DOWN_Y = SWORD_HEIGHT;
  static final double SWORD_UP_X = SWORD_WIDTH;
  static final double SWORD_UP_Y = 0;
  static final double SWORD_LEFT_X = 3*SWORD_WIDTH;
  static final double SWORD_LEFT_Y = SWORD_HEIGHT;
  static final double SWORD_RIGHT_X = 3*SWORD_WIDTH;
  static final double SWORD_RIGHT_Y = 0;
  static final String SWORD_IMAGE = "sword.png";
   
  // Items
  static final String KEY_IMAGE = "KeyIcons.png";
  final static double KEY_WIDTH = 32;
  final static double KEY_HEIGHT = 32;
  
  final static double GOLD_KEY_X = KEY_WIDTH;
  final static double WOODEN_KEY_X = 0;
  final static double SILVER_KEY_X = 3*KEY_WIDTH;
  final static double HEART_KEY_X = 2*KEY_WIDTH;
  
  final static double POTION_WIDTH = 16;
  final static double POTION_HEIGHT = 16;
  final static double POTION_X = POTION_WIDTH;
  final static double POTION_Y = 8*POTION_HEIGHT;
  final static String POTION_IMAGE = "open_tileset.png";
  
  final static double ITEM_WIDTH = 32;
  final static double ITEM_HEIGHT = 32;
  
  // slime 
  static final double SLIME_INIT_X = INIT_APP_WIDTH/4;
  static final double SLIME_INIT_Y = INIT_APP_HEIGHT / 4;
  
  static final double SLIME_WIDTH = 32;
  static final double SLIME_HEIGHT = 32;
  static final double SLIME_JUMP = 2*SLIME_HEIGHT;
  static final double SLIME_DIE = 4*SLIME_HEIGHT;
  static final int SLIME_FRAMES = 10;
  
  static final String GREEN_SLIME_IMAGE = "green_slime.png";
  static final String BLUE_SLIME_IMAGE = "blue_slime.png"; 
  static final String RED_SLIME_IMAGE = "red_slime.png"; 
  static final String YELLOW_SLIME_IMAGE = "yellow_slime.png"; 
  
  
  // Obstacles
  static final double BUSH_HEIGHT = 16;
  static final double BUSH_WIDTH = 16;
  static final double BUSH_X = 5*BUSH_WIDTH;
  static final double BUSH_Y = 2*BUSH_HEIGHT;
 
  static final double ROCK_HEIGHT = 16;
  static final double ROCK_WIDTH = 16;
  static final double ROCK_X = 5*ROCK_WIDTH;
  static final double ROCK_Y = 3*ROCK_HEIGHT;
  
  static final double CHEST_HEIGHT = 16;
  static final double CHEST_WIDTH = 16;
  static final double CHEST_OPENED_X = CHEST_WIDTH;
  static final double CHEST_OPENED_Y = 5*CHEST_HEIGHT;
  static final double CHEST_CLOSED_X = 0;
  static final double CHEST_CLOSED_Y = 5*CHEST_HEIGHT;
  
  static final String OBSTACLE_IMAGE = "open_tileset.png";
  
  // Background
  static final double GRASS_HEIGHT = 16;
  static final double GRASS_WIDTH = 16;
  static final double GRASS_X = 0;
  static final double GRASS_Y = 0;
  
  static final String GRASS_IMAGE = "grass.png";
  static final String BG_IMG = "bg.png";
}
