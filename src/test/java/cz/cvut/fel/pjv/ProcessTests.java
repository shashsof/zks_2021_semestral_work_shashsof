
package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import junitparams.FileParameters;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author sonia
 */
/*
If i used junit5 there will be 
@Tag("Process Tests")
*/
@RunWith(Parameterized.class)
public class ProcessTests {
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static Level level;
    static ObjectMapper objectMapper;
    static List<Sprite> inventory;
    static Random rand = new Random();
    static UserActionHandler uah;
    
    public ProcessTests() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
        uah = new UserActionHandler(spriteManager, glh);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        glh.resetVariables();
        spriteManager.clearAllSprites();
    }

    //1
    @Test
    public void Gaming() throws IOException{
        System.out.println("test simple gaming - hero go right and pick the key");
        glh.initSprites("level1.json");
        KeyEvent key = new KeyEvent(KeyEvent.KEY_PRESSED, "RIGHT", "RIGHT", KeyCode.RIGHT, false, false, false, false);
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        while(hero.getX() < INIT_APP_WIDTH - hero.getFw()){
            uah.handle(key);
            glh.updateSprites();
        }
        key = new KeyEvent(KeyEvent.KEY_RELEASED, "RIGHT", "RIGHT", KeyCode.RIGHT, false, false, false, false);
        uah.handle(key);
        glh.checkPickingItem();
        int result = gameVariables.get("GOLD_KEY");
        assertEquals(1, result);
    }
    
    //2
    @Test
    public void Gaming2() throws IOException{
        int cnt = 0;
        System.out.println("test simple gaming - hero moves and kill slime");
        glh.initSprites("level1.json");
        KeyEvent key = new KeyEvent(KeyEvent.KEY_PRESSED, "RIGHT", "RIGHT", KeyCode.RIGHT, false, false, false, false);
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        while(cnt < 10){
            uah.handle(key);
            glh.updateSprites();
            cnt++;
        }
        cnt = 0;
        key = new KeyEvent(KeyEvent.KEY_RELEASED, "RIGHT", "RIGHT", KeyCode.RIGHT, false, false, false, false);
        uah.handle(key);
        key = new KeyEvent(KeyEvent.KEY_PRESSED, "UP", "UP", KeyCode.UP, false, false, false, false);
        while(cnt < 5){
            uah.handle(key);
            glh.updateSprites();
            cnt++;
        }
        cnt = 0;
        key = new KeyEvent(KeyEvent.KEY_RELEASED, "UP", "UP", KeyCode.UP, false, false, false, false);
        uah.handle(key);
        key = new KeyEvent(KeyEvent.KEY_PRESSED, "SPACE", "SPACE", KeyCode.SPACE, false, false, false, false);
        uah.handle(key);
        glh.updateSprites();
        slime slime = (slime)spriteManager.getSprite(SpriteId.SLIME_RED);
        slime.setPosition(690-16, 295-64-1);
        glh.checkAttack();
        assertTrue(slime.isDied());
    }
    
    //3
    @Test
    public void Gaming3() throws IOException{
        System.out.println("simple gaming test - hero moves and open chest");
        glh.initSprites("level1.json");
        KeyEvent key = new KeyEvent(KeyEvent.KEY_PRESSED, "DOWN", "DOWN", KeyCode.DOWN, false, false, false, false);
        Hero hero = (Hero)spriteManager.getSprite(SpriteId.HERO);
        hero.setPosition(600, hero.getY());
        while(hero.getY() < INIT_APP_HEIGHT - hero.getFh()- 2 * CHEST_HEIGHT -7){
            uah.handle(key);
            glh.updateSprites();
        }
        key = new KeyEvent(KeyEvent.KEY_RELEASED, "DOWN", "DOWN", KeyCode.DOWN, false, false, false, false);
        uah.handle(key);
        Sprite silverKey = new Sprite(SpriteId.SILVER_KEY, KEY_IMAGE, SILVER_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
        silverKey.setGroupName("ITEM");
        glh.putToInventory(silverKey);
        glh.openChest();
        int result = gameVariables.get("HEART_KEY");
        assertEquals(1, result);
    }
    
    //4
    /*@Test
    @FileParameters("data.csv")
    public void loadParamsFromFile1(boolean collision, int positionX) {
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(positionX, 10);
        spriteManager.addSprite(hero);
        Sprite chest = new Obstacle(SpriteId.BUSH1, OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
        if(collision){
           chest.setPosition(positionX, 10); 
        }else{
            chest.setPosition(positionX+3*HERO_WIDTH, 10+3*HERO_WIDTH); 
        }
        chest.setGroupName("BUSHES");
        spriteManager.addSprite(chest);
        assertEquals(collision, glh.checkObstacle(hero));
    }
    
    //5
    @Test
    @FileParameters("data2.csv")
    public void loadParamsFromFile2(boolean collision, int positionX, int positionY) {
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(positionX, positionY);
        spriteManager.addSprite(hero);
        Sprite chest = new Obstacle(SpriteId.BUSH1, OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
        if(collision){
           chest.setPosition(positionX, positionY); 
        }else{
            chest.setPosition(positionX+3*HERO_WIDTH, positionY+3*HERO_WIDTH); 
        }
        chest.setGroupName("BUSHES");
        spriteManager.addSprite(chest);
        assertEquals(collision, glh.checkObstacle(hero));
    }
    
    //6
    @Test
    @FileParameters("data3.csv")
    public void loadParamsFromFile3( int positionX, int positionY, boolean nearChest, String chestState, String correctKey) {
        
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(positionX, positionY);
        spriteManager.addSprite(hero);
        
        if(correctKey == "true" && chestState == "closed"){
            Sprite key = new Sprite(SpriteId.GOLD_KEY, KEY_IMAGE, GOLD_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
            key.setGroupName("ITEM");
            glh.putToInventory(key);
        }else if(correctKey == "false"){
            Sprite key = new Sprite(SpriteId.SILVER_KEY, KEY_IMAGE, SILVER_KEY_X, 0, KEY_WIDTH, KEY_HEIGHT, KEY_WIDTH, KEY_HEIGHT, 0, false);
            key.setGroupName("ITEM");
            glh.putToInventory(key);
        }//else: nothing
        
        Sprite chest = new Obstacle(SpriteId.CHEST1, OBSTACLE_IMAGE, CHEST_CLOSED_X, CHEST_CLOSED_Y, CHEST_WIDTH, CHEST_HEIGHT, 2 * CHEST_WIDTH, 2 * CHEST_HEIGHT, 0, false);
        if(nearChest){
           chest.setPosition(positionX, positionY); 
        }else{
            chest.setPosition(positionX+3*HERO_WIDTH, positionY+3*HERO_WIDTH); 
        }
        chest.setGroupName("CHEST");
        spriteManager.addSprite(chest);
        glh.openChest();
        int result = gameVariables.get("CHEST1");
        int expect;
        if(nearChest && chestState == "closed" && correctKey == "true"){
            expect = 1;
        }else{
            expect = 0;
        }
        assertEquals(expect, result);
    }
    
    //7
    @Test
    @Parameters({"true, 200, 200", "false, -1, -1", "false, 250, 10", "false, 1230, 700" })
    public void whenWithAnnotationProvidedParams_thenSafeAdd(boolean collision, int positionX, int positionY) {
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(positionX, positionY);
        spriteManager.addSprite(hero);
        Sprite chest = new Obstacle(SpriteId.BUSH1, OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
        if(collision){
           chest.setPosition(positionX, positionY); 
        }else{
            chest.setPosition(positionX+3*HERO_WIDTH, positionY+3*HERO_WIDTH); 
        }
        chest.setGroupName("BUSHES");
        spriteManager.addSprite(chest);
        assertEquals(collision, glh.checkObstacle(hero));
    }*/
}
