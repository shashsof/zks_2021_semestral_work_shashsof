/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv;

/**
 *
 * @author sonia
 */
public class Obstacle extends Sprite{
   
    public Obstacle(SpriteId id, String imageName, 
            double sourcex, 
            double sourcey, 
            double source_width,
            double source_height,
            double final_width,
            double final_height,
            int frames,
            boolean anim) {
    super(id, imageName, sourcex, sourcey, source_width, source_height, final_width, final_height, frames, anim);
    } 
}
