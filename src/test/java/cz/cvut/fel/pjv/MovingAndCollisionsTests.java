/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.pjv;

import com.fasterxml.jackson.databind.ObjectMapper;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.glh;
import static cz.cvut.fel.pjv.GameLoopHandlerTest.spriteManager;
import static cz.cvut.fel.pjv.Util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Sofie
 */
/*
If i used junit5 there will be 
@Tag("Moving and Collisions Tests")
*/
public class MovingAndCollisionsTests {
    
    static GameLoopHandler glh;
    static SpriteManager spriteManager;
    static Map<String, Integer> gameVariables;
    static Level level;
    static ObjectMapper objectMapper;
    static List<Sprite> inventory;
    static Random rand = new Random();
    
    public MovingAndCollisionsTests() {
        
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        spriteManager = new SpriteManager();
        gameVariables = new HashMap<>();
        glh = new GameLoopHandler(spriteManager, gameVariables);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        glh.resetVariables();
        spriteManager.clearAllSprites();
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    //1
    //@Tag("Moving")
    @Test
    public void testUpdateSpritesSucces(){
        System.out.println("moving sprites");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(200, 200);
        spriteManager.addSprite(hero);
        Sword sword = new Sword(SpriteId.SWORD, SWORD_IMAGE, SWORD_DOWN_X, SWORD_DOWN_Y, SWORD_WIDTH, SWORD_HEIGHT, 2*SWORD_WIDTH, 2*SWORD_HEIGHT, 0, false);
        sword.setHidden(false);
        sword.setPosition(hero.getX()-8, hero.getY()+2*HERO_HEIGHT-5);
        spriteManager.addSprite(sword);
        hero.setSpeedX(5);
        glh.updateSprites();
        int result = (int)hero.getX();
        assertEquals(205, result);
    }
    
    //2
    //@Tag("Moving")
    @Test
    public void testUpdateSpritesFail(){
        System.out.println("fail update sprites");
        thrown.expect(NullPointerException.class);
        glh.updateSprites();
    }
    
    //3
    //@Tag("Moving")
    @Test
    public void testEndOfScreenTrue(){
        System.out.println("hero goes out of screen");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(-1, -1);
        spriteManager.addSprite(hero);
        glh.endOfScreen();
        int result = (int)hero.getX();
        assertEquals(0, result);
    }
    
    //4
    //@Tag("Moving")
    @Test
    public void testEndOfScreenFalse(){
        System.out.println("hero is on screen");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(10, 10);
        spriteManager.addSprite(hero);
        glh.endOfScreen();
        int result = (int)hero.getX();
        assertEquals(10, result);
    }
    
    //5
    //@Tag("Collision")
    @Test
    public void CheckObstacleTrue(){
        System.out.println("testing hero and obstacle collision - true");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(201, 200);
        spriteManager.addSprite(hero);
        Sprite chest = new Obstacle(SpriteId.BUSH1, OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
        chest.setPosition(232, 200);
        chest.setGroupName("BUSHES");
        spriteManager.addSprite(chest);
        glh.checkObstacle(hero);
        int result = (int)hero.getX();
        assertEquals(200, result);
    }
    
    //6
    //@Tag("Collision")
    @Test
    public void CheckObstacleFalse(){
        System.out.println("testing hero and obstacle collision - false");
        Hero hero = new Hero(SpriteId.HERO, HERO_IMAGE, HERO_STAND, HERO_DOWN, HERO_WIDTH, HERO_HEIGHT, 2*HERO_WIDTH, 2*HERO_HEIGHT, HERO_FRAMES, false);
        hero.setPosition(201, 200);
        spriteManager.addSprite(hero);
        Sprite chest = new Obstacle(SpriteId.BUSH1, OBSTACLE_IMAGE, BUSH_X, BUSH_Y, BUSH_WIDTH, BUSH_HEIGHT, 2 * BUSH_WIDTH, 2 * BUSH_HEIGHT, 0, false);
        chest.setPosition(233, 200);
        chest.setGroupName("BUSHES");
        spriteManager.addSprite(chest);
        glh.checkObstacle(hero);
        int result = (int)hero.getX();
        assertEquals(201, result);
    }
}
