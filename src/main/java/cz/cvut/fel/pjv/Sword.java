package cz.cvut.fel.pjv;

import javafx.geometry.Rectangle2D;

/**
 *
 * @author sofia
 */
public class Sword extends Sprite {
    private String direction; //which way is sword pointing.
    private boolean hidden; // if sword is visible.
    
    
    public Sword(SpriteId id, String imageName, 
            double sourcex, 
            double sourcey, 
            double source_width,
            double source_height,
            double final_width,
            double final_height,
            int frames,
            boolean anim) {
    super(id, imageName, sourcex, sourcey, source_width, source_height, final_width, final_height, frames, anim);
    direction = "down";
    hidden = true;
     }
     
     public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
    
    /**
     * Bounds are more approximate to a sword appereance.
     * @return rectangle2D
     */
    @Override
    public Rectangle2D getBounds(){
        if(this.direction.equals("up") || this.direction.equals("down")){
            return new Rectangle2D(getX()+12, getY(), 8, getFh());
        }else{  // left or right
            return new Rectangle2D(getX(), getY()+12, getFw(), 8);
        }
    }
}
